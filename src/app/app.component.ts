import { Component } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ApiService]
})
export class AppComponent {
  public classMenu:string;
  public post:any;
  public loading:boolean = false;
  public postImg: string;
  
  constructor(
    private _ApiService: ApiService
    ) {
  }

  
  ngOnInit() {
  } 

  postID(id:number){
    let page:number = id;
    this.loading= true;
    this._ApiService.getPost(page).subscribe(
      (result) => {
        this.post = result;
        this.loading = false;

        if(this.classMenu == "step-home"){
          this.postImg = 'front-end_info_home.png';
        }
        if(this.classMenu == "step-search"){
          this.postImg = 'front-end_info_search.svg';
        }
        if(this.classMenu == "step-cel"){
          this.postImg = 'front-end_info_celus.png';
        }
        console.log(this.post);
      }
    );
  }
}
