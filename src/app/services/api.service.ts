import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(private _http:HttpClient) { }
  
  getPost(page){
    return this._http.get('https://jsonplaceholder.typicode.com/posts/'+page);
  }
}
